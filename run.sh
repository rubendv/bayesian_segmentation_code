#!/usr/bin/env bash

mkdir -p /scratch/rubendv/bayesian/

docker pull rubendv-pc:5000/bayesian && time docker run -ti --rm -v /scratch/rubendv/originals_4y_1d:/input/originals_4y_1d:r -v /scratch/rubendv/originals_3m_1h:/input/originals_3m_1h:r -v /scratch2/benjmam/ruben_paper/winter_2011:/input/spoca/3m_1h:r -v /scratch2/benjmam/ruben_paper/2010_2014:/input/spoca/4y_1d:r -v /scratch/rubendv/bayesian:/output:rw rubendv-pc:5000/bayesian "$@"
