FROM ubuntu:14.04

# Install apt packages
RUN apt-get update \
&& apt-get -y upgrade \
&& apt-get -y install python-dev python-pip git gfortran libatlas-base-dev libpng-dev libfreetype6-dev libpng12-dev pkg-config
RUN pip install -U numpy \
&& pip install -U scipy \
&& pip install -U cython \
&& pip install -U matplotlib joblib jinja2 pandas scikit-image \
&& pip install dill scikit-learn \
&& pip install git+https://github.com/astropy/astropy.git \
&& pip install git+https://github.com/sunpy/sunpy.git \
&& mkdir -p /opt && git clone -b rotate https://github.com/rubendv/dfresampling.git /opt/dfresampling && pip install -e /opt/dfresampling

#RUN export uid=1000 gid=1000 && \
RUN export uid=3652 gid=342 && \
    mkdir -p /home/local && \
    echo "local:x:${uid}:${gid}:local,,,:/home/local:/bin/bash" >> /etc/passwd && \
    echo "local:x:${gid}:" >> /etc/group && \
    echo "local ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/local && \
    chmod 0440 /etc/sudoers.d/local && \
    chown ${uid}:${gid} -R /home/local

RUN mkdir -p /input /output

USER local
ENV HOME /home/local
WORKDIR /home/local
RUN python -c "import sunpy; print sunpy.__version__"
COPY masks /home/local/masks
COPY test_masks /home/local/test_masks
COPY src /home/local/src
