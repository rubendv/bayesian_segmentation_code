from estimate_likelihoods import em_iteration, calculate_kernels, load_masks
from segmentation import segment_dataset, segmentation
from joblib import Memory, Parallel, delayed, dump, load
from util import memory_wrapper, mkdir_p
import matplotlib.pyplot as plt
import dill
import sunpy.cm
import numpy as np
from glob import glob
import os
from astropy.io import fits
import sunpy.wcs
import sunpy.map

if __name__ == "__main__":
    cache_path = '/output/cache'
    mkdir_p(cache_path)
    memory = Memory(cachedir=cache_path, verbose=0, compress=3)
    
    masks = load_masks(*["masks/limbcorrected_00000928.xcf.{}.png".format(c) for c in ["ar", "qs", "ch"]])
    preprocessed = load("/output/preprocessed/4y_1d/20130423_000006.jl")
    num_classes = len(masks)
    plot_gamma = 0.5
    
    mkdir_p("/output/plots")

    print("Plotting manual segmentation")
    plt.figure(figsize=(4,4))
    plt.imshow(preprocessed**plot_gamma, cmap='gray', origin="lower")
    for i in range(len(masks)):
        plt.contour(masks[i], [0.5], colors=["r","g","c"][i])
    plt.axis('off')
    plt.tight_layout(pad=0, h_pad=0, w_pad=0)
    plt.subplots_adjust(wspace=-0.2, hspace=-0.2, bottom=-0.05, left=-0.05, top=1.05, right=1.05)
    plt.savefig("/output/plots/manual_segmentation.pdf", dpi=600)

    print("Calculating KDE of intensity likelihoods")
    num_classes = 3
    intensity_kernels, vmin, vmax = calculate_kernels(
        masks=masks,
        preprocessed=preprocessed
    )
    class_likelihoods = np.ones(num_classes)/num_classes
    latitude_likelihoods = np.ones((num_classes, 512))/180

    
    from util import get_date, spoca_filling_factor, is_anomaly

    print('Gathering SPoCA data')

    original_paths = sorted(glob('/input/originals_3m_1h/*.fits'))
    preprocessed_paths = sorted(glob('/output/preprocessed/3m_1h/*.jl'))
    spoca_ar_paths = sorted(glob('/input/spoca/3m_1h/AR_maps/*.fits'))
    spoca_ch_paths = sorted(glob('/input/spoca/3m_1h/CH_maps_min3days/*.fits'))
    anomalies = [memory_wrapper(memory, is_anomaly, path) for path in original_paths]
    bayes_dates = [memory_wrapper(memory, get_date, path) for path in original_paths]
    spoca_dates = {'AR': [memory_wrapper(memory, get_date, path) for path in spoca_ar_paths], 'CH': [memory_wrapper(memory, get_date, path) for path in spoca_ch_paths]}
    spoca_filling_factors = {
        'AR': Parallel(n_jobs=-1)([delayed(memory_wrapper)(memory, spoca_filling_factor, path) for path in spoca_ar_paths]),
        'CH': Parallel(n_jobs=-1)([delayed(memory_wrapper)(memory, spoca_filling_factor, path) for path in spoca_ch_paths]),
    }

    intensity_kernels_path = '/output/intensity_kernels.jl'
    with open(intensity_kernels_path, 'w+') as f:
        dill.dump(intensity_kernels, f)

    print("Estimating class and latitude likelihoods using EM-iteration for 4y_1d dataset")
    original_paths = sorted(glob('/input/originals_4y_1d/*.fits'))
    preprocessed_paths = sorted(glob('/output/preprocessed/4y_1d/*.jl'))
    class_likelihoods, latitude_likelihoods = memory_wrapper(memory, em_iteration,
        memory, 
        original_paths, 
        preprocessed_paths, 
        destination_prefix='/output/likelihoods_estimation/4y_1d', 
        intensity_kernels_path=intensity_kernels_path, 
        intensity_vmin=vmin, 
        intensity_vmax=vmax
    )

    print('Class likelihoods: {}'.format(class_likelihoods))

    dump(class_likelihoods, '/output/likelihoods_estimation/4y_1d/class_likelihoods.jl', compress=True)
    dump(latitude_likelihoods, '/output/likelihoods_estimation/4y_1d/latitude_likelihoods.jl', compress=True)

    print("Plotting joint intensity-latitude likelihoods")
    plt.figure()
    kernel_info = [("AR", "r-"), ("QS", "g-"), ("CH", "c-")]
    for likelihood, (name, style) in zip(latitude_likelihoods, kernel_info):
        plt.plot(np.linspace(-90, 90, len(likelihood)), likelihood, style, label=name)
    plt.xlim([-90, 90])
    plt.xlabel("Latitude (degrees)")
    plt.ylabel("Probability density function")
    plt.legend(loc="best")
    plt.tight_layout()
    plt.savefig("/output/plots/latitude_likelihood_4y_1d.pdf")

    print("Plotting contours of 4y_1d dataset")
    segment_dataset(
        memory,
        original_paths,
        preprocessed_paths,
        '/output/map/4y_1d/',
        intensity_kernels_path,
        class_likelihoods,
        latitude_likelihoods,
        vmin,
        vmax,
        properties_to_save=['contours', 'latitudes', 'segmentation']
    )

    original_paths = sorted(glob('/input/originals_3m_1h/*.fits'))
    preprocessed_paths = sorted(glob('/output/preprocessed/3m_1h/*.jl'))

    #print("Estimating class and latitude likelihoods using EM-iteration for 3m_1h dataset")
    #class_likelihoods, latitude_likelihoods = memory_wrapper(memory, em_iteration,
    #    memory, 
    #    original_paths, 
    #    preprocessed_paths, 
    #    destination_prefix='/output/likelihoods_estimation/3m_1h', 
    #    intensity_kernels_path=intensity_kernels_path, 
    #    intensity_vmin=vmin, 
    #    intensity_vmax=vmax
    #)

    print("Plotting contours and calculating areas for 3m_1h dataset")
    segment_dataset(
        memory,
        original_paths,
        preprocessed_paths,
        '/output/map/3m_1h/',
        intensity_kernels_path,
        class_likelihoods,
        latitude_likelihoods,
        vmin,
        vmax,
        properties_to_save=['contours', 'areas']
    )

    #spoca_paths = {}
    #class_directory = {'ar':'AR_maps', 'ch': 'CH_maps_min3days'}
    #def spoca_directory(cls, dataset):
    #    return '/input/spoca/{dataset}/{directory}'.format(dataset=dataset, directory=class_directory[cls])
    #for dataset in ['4y_1d', '3m_1h']:
    #    spoca_paths[dataset] = {}
    #    for cls in ['ar', 'ch']
    #        spoca_paths[dataset][cls] = sorted(glob(os.path.join(spoca_directory(cls, dataset), '*.fits')))

    
    ar_mask = fits.open('/input/spoca/4y_1d/AR_maps/20130423_000000.ARMap.fits')[1].data > 0
    ch_mask = fits.open('/input/spoca/4y_1d/CH_maps_min3days/20130423_000007.CHMap.fits')[1].data > 0
    qs_mask = (~ar_mask) & (~ch_mask)
    spoca_masks = [ar_mask[::4,::4], qs_mask[::4,::4], ch_mask[::4,::4]]
    
    print("Plotting SPoCA mask")
    plt.figure(figsize=(4,4))
    plt.imshow(preprocessed**plot_gamma, cmap='gray', origin="lower")
    for i in [1, 0, 2]:
        plt.contour(spoca_masks[i], [0.5], colors=["r","g","c"][i])
    plt.axis('off')
    plt.tight_layout(pad=0, h_pad=0, w_pad=0)
    plt.subplots_adjust(wspace=-0.2, hspace=-0.2, bottom=-0.05, left=-0.05, top=1.05, right=1.05)
    plt.savefig("/output/plots/spoca_mask.pdf", dpi=600)

    my_intensity_kernels, my_vmin, my_vmax = intensity_kernels, vmin, vmax

    print("Calculating KDE of SPoCA intensity likelihoods")
    num_classes = 3
    intensity_kernels, vmin, vmax = calculate_kernels(
        masks=spoca_masks,
        preprocessed=preprocessed
    )
    
    print("Plotting KDE of intensity likelihoods")
    plt.figure()
    kernel_info = [("Manual AR", "r-"), ("Manual QS", "g-"), ("Manual CH", "c-")]
    x = np.linspace(0, 1, 1000)
    kernel_info = [("AR", "r-", 'r--'), ("QS", "g-", 'g--'), ("CH", "c-", 'c--')]
    for i, (name, style, spoca_style) in enumerate(kernel_info):
        plt.plot(x, my_intensity_kernels[i](x), style, label='Manual {}'.format(name))
        plt.plot(x, intensity_kernels[i](x), spoca_style, label='SPoCA {}'.format(name))
    plt.xscale("log")
    #plt.ylim([0, 13])
    #plt.ylim([0, np.max(kernels[1](x))])
    plt.xlabel("Normalized intensity")
    plt.ylabel("Probability density function")
    plt.legend(loc="best")
    plt.tight_layout()
    plt.savefig("/output/plots/intensity_likelihood.pdf")

    mkdir_p('/output/spoca')
    my_intensity_kernels_path = intensity_kernels_path
    intensity_kernels_path = '/output/spoca/intensity_kernels.jl'
    with open(intensity_kernels_path, 'w+') as f:
        dill.dump(intensity_kernels, f)

    #print("Making ML segmentation of 4y_1d using SPoCA masks")
    #spoca_class_likelihoods = np.ones(num_classes)/num_classes
    #spoca_latitude_likelihoods = np.ones((num_classes, 512))/180
    #original_paths = sorted(glob('/input/originals_4y_1d/*.fits'))
    #preprocessed_paths = sorted(glob('/output/preprocessed/4y_1d/*.jl'))
    #segment_dataset(
    #    memory,
    #    original_paths,
    #    preprocessed_paths,
    #    '/output/spoca/ml/3m_1h/',
    #    intensity_kernels_path,
    #    spoca_class_likelihoods,
    #    spoca_latitude_likelihoods,
    #    vmin,
    #    vmax,
    #    properties_to_save=['contours', 'areas']
    #)

    
    print("Estimating class and latitude likelihoods using EM-iteration for 4y_1d dataset using SPoCA masks")
    original_paths = sorted(glob('/input/originals_4y_1d/*.fits'))
    preprocessed_paths = sorted(glob('/output/preprocessed/4y_1d/*.jl'))
    spoca_class_likelihoods, spoca_latitude_likelihoods = memory_wrapper(memory, em_iteration,
        memory, 
        original_paths, 
        preprocessed_paths, 
        destination_prefix='/output/spoca/4y_1d', 
        intensity_kernels_path=intensity_kernels_path, 
        intensity_vmin=vmin, 
        intensity_vmax=vmax
    )
    print("SPoCA class likelihoods: {}".format(spoca_class_likelihoods))

    print('Segmenting for core/extended comparison figures')
    segmentation(memory, '/input/originals_4y_1d/AIA20110308_0000_0193.fits', '/output/preprocessed/4y_1d/20110308_000007.jl', '/output/figure_segmentations/spoca_map', intensity_kernels_path, spoca_class_likelihoods, spoca_latitude_likelihoods, 0, 1, ['contours', 'membership', 'segmentation'])
    segmentation(memory, '/input/originals_4y_1d/AIA20110308_0000_0193.fits', '/output/preprocessed/4y_1d/20110308_000007.jl', '/output/figure_segmentations/my_map', my_intensity_kernels_path, class_likelihoods, latitude_likelihoods, 0, 1, ['contours', 'membership', 'segmentation'])
    segmentation(memory, '/input/originals_4y_1d/AIA20110308_0000_0193.fits', '/output/preprocessed/4y_1d/20110308_000007.jl', '/output/figure_segmentations/my_ml', my_intensity_kernels_path, np.ones(3)/3.0, np.ones(latitude_likelihoods.shape)/180, 0, 1, ['contours', 'membership', 'segmentation'])
    
    print('Making core/extended comparison figures')
    image = load('/output/preprocessed/4y_1d/20110308_000007.jl')
    image = image**plot_gamma
    my_ml_segmentation = load('/output/figure_segmentations/my_ml/segmentation.jl')
    my_map_segmentation = load('/output/figure_segmentations/my_map/segmentation.jl')
    spoca_map_segmentation = load('/output/figure_segmentations/spoca_map/segmentation.jl')
    my_ml_membership = load('/output/figure_segmentations/my_ml/membership.jl')
    my_map_membership = load('/output/figure_segmentations/my_map/membership.jl')
    spoca_map_membership = load('/output/figure_segmentations/spoca_map/membership.jl')
    
    fig = plt.figure(figsize=(8,8))
    fig.add_subplot(221)
    plt.imshow(image, cmap='gray', origin="lower")
    plt.contour(my_map_segmentation == 0, [0.5], colors="r")
    plt.contour(my_map_segmentation == 2, [0.5], colors="c")
    plt.gca().set_axis_off()
    fig.add_subplot(222)
    plt.imshow(image, cmap='gray', origin="lower")
    plt.contour(spoca_map_segmentation == 0, [0.5], colors="r")
    plt.contour(spoca_map_segmentation == 2, [0.5], colors="c")
    plt.gca().set_axis_off()
    fig.add_subplot(223)
    plt.imshow(my_map_membership[:,:,0], cmap="gray", origin="lower")
    plt.gca().set_axis_off()
    fig.add_subplot(224)
    plt.imshow(spoca_map_membership[:,:,0], cmap="gray", origin="lower")
    plt.gca().set_axis_off()
    plt.tight_layout(pad=0, h_pad=0, w_pad=0)
    plt.subplots_adjust(wspace=-0.2, hspace=-0.2, bottom=-0.05, left=-0.05, top=1.05, right=1.05)
    plt.savefig("/output/plots/diffuse_core_comparison.png", dpi=600)

    print('Segmenting for ml/map comparison figures')
    segmentation(memory, '/input/originals_4y_1d/AIA20130423_0000_0193.fits', '/output/preprocessed/4y_1d/20130423_000006.jl', '/output/figure_segmentations/my_map', my_intensity_kernels_path, class_likelihoods, latitude_likelihoods, 0, 1, ['contours', 'membership', 'segmentation'])
    segmentation(memory, '/input/originals_4y_1d/AIA20130423_0000_0193.fits', '/output/preprocessed/4y_1d/20130423_000006.jl', '/output/figure_segmentations/my_ml', my_intensity_kernels_path, np.ones(3)/3.0, np.ones(latitude_likelihoods.shape)/180, 0, 1, ['contours', 'membership', 'segmentation'])
    
    print('Making ml/map comparison figures')
    image = load('/output/preprocessed/4y_1d/20130423_000006.jl')
    image = image**plot_gamma
    my_ml_segmentation = load('/output/figure_segmentations/my_ml/segmentation.jl')
    my_map_segmentation = load('/output/figure_segmentations/my_map/segmentation.jl')
    spoca_map_segmentation = load('/output/figure_segmentations/spoca_map/segmentation.jl')
    my_ml_membership = load('/output/figure_segmentations/my_ml/membership.jl')
    my_map_membership = load('/output/figure_segmentations/my_map/membership.jl')
    spoca_map_membership = load('/output/figure_segmentations/spoca_map/membership.jl')
    

    fig = plt.figure(figsize=(8,8))
    fig.add_subplot(221)
    plt.imshow(image, cmap='gray', origin="lower")
    plt.contour(my_ml_segmentation == 0, [0.5], colors="r")
    plt.contour(my_ml_segmentation == 2, [0.5], colors="c")
    plt.gca().set_axis_off()
    fig.add_subplot(222)
    plt.imshow(image, cmap='gray', origin="lower")
    plt.contour(my_map_segmentation == 0, [0.5], colors="r")
    plt.contour(my_map_segmentation == 2, [0.5], colors="c")
    plt.gca().set_axis_off()
    fig.add_subplot(223)
    plt.imshow(my_ml_membership[:,:,2], cmap="gray", origin="lower")
    plt.gca().set_axis_off()
    fig.add_subplot(224)
    plt.imshow(my_map_membership[:,:,2], cmap="gray", origin="lower")
    plt.gca().set_axis_off()
    plt.tight_layout(pad=0, h_pad=0, w_pad=0)
    plt.subplots_adjust(wspace=-0.2, hspace=-0.2, bottom=-0.05, left=-0.05, top=1.05, right=1.05)
    plt.savefig("/output/plots/ml_map_comparison.png", dpi=600)

    print('CH membership variance ML: {}'.format(np.nanvar(np.ma.masked_greater(my_ml_membership[:,:,2], 0.50).compressed())))
    print('CH membership mean ML: {}'.format(np.nanmean(np.ma.masked_greater(my_ml_membership[:,:,2], 0.50).compressed())))
    print('CH membership variance MAP: {}'.format(np.nanvar(np.ma.masked_greater(my_map_membership[:,:,2], 0.50).compressed())))
    print('CH membership mean MAP: {}'.format(np.nanmean(np.ma.masked_greater(my_map_membership[:,:,2], 0.50).compressed())))

    mkdir_p('/output/spoca/4y_1d')
    dump(spoca_class_likelihoods, '/output/spoca/4y_1d/class_likelihoods.jl', compress=True)
    dump(spoca_latitude_likelihoods, '/output/spoca/4y_1d/latitude_likelihoods.jl', compress=True)

    print("Plotting latitude likelihoods")
    plt.figure()
    kernel_info = [("AR", "r-", 'r--'), ("QS", "g-", 'g--'), ("CH", "c-", 'c--')]
    for i, (name, style, spoca_style) in enumerate(kernel_info):
        x = np.linspace(-90, 90, len(latitude_likelihoods[i]))
        plt.plot(x, latitude_likelihoods[i], style, label='Manual {}'.format(name))
        plt.plot(x, spoca_latitude_likelihoods[i], spoca_style, label='SPoCA {}'.format(name))
    plt.xlim([-90, 90])
    plt.xlabel("Latitude (degrees)")
    plt.ylabel("Probability density function")
    plt.legend(loc="best")
    plt.tight_layout()
    plt.savefig("/output/plots/latitude_likelihood_4y_1d.pdf")
    
    print("Plotting contours and calculating areas for SPoCA 3m_1h dataset")
    original_paths = sorted(glob('/input/originals_3m_1h/*.fits'))
    preprocessed_paths = sorted(glob('/output/preprocessed/3m_1h/*.jl'))
    segment_dataset(
        memory,
        original_paths,
        preprocessed_paths,
        '/output/spoca/map/3m_1h/',
        intensity_kernels_path,
        spoca_class_likelihoods,
        spoca_latitude_likelihoods,
        vmin,
        vmax,
        properties_to_save=['contours', 'areas']
    )

    areas = {}
    datasets = {'Our method: Manual mask': {'paths': sorted(glob('/output/map/3m_1h/*/areas.jl'))}, 'Our method: SPoCA mask': {'paths': sorted(glob('/output/spoca/map/3m_1h/*/areas.jl'))}}
    for name, dataset in datasets.items():
        areas[name] = np.zeros((len(dataset['paths']), 3))
        for i, area_path in enumerate(dataset['paths']):
            if anomalies[i]:
                areas[name][i] = np.nan
            else:
                areas[name][i] = load(area_path)
    
    print("Drawing area comparison plots")
    
    for class_name, class_index in ('AR', 0), ('CH', 2):
        plt.figure()
        for dataset_name, dataset_areas in areas.items():
            plt.plot(bayes_dates, dataset_areas[...,class_index], label='{} {}'.format(dataset_name, class_name))
        plt.plot(spoca_dates[class_name], spoca_filling_factors[class_name], label='SPoCA {}'.format(class_name))
        plt.legend(loc='best')
        plt.xlabel('Time')
        plt.ylabel('Area coverage')
        plt.gcf().autofmt_xdate()
        plt.savefig('/output/plots/spoca_comparison_{}.pdf'.format(class_name))

    def rolling_window(a, window):
        shape = a.shape[:-1] + (a.shape[-1] - window + 1, window)
        strides = a.strides + (a.strides[-1],)
        return np.lib.stride_tricks.as_strided(a, shape=shape, strides=strides)

    from scipy.stats import nanmedian
    ch_areas = areas['Our method: Manual mask'][...,2]
    median_daily_variance = nanmedian(np.nanvar(rolling_window(ch_areas, 24), axis=-1))
    total_variance = np.nanvar(ch_areas)
    print('Median daily CH area variance: {}'.format(median_daily_variance))
    print('Total CH area variance: {}'.format(total_variance))
    dump({'median_daily': median_daily_variance, 'total': total_variance}, '/output/ch_area_variance.jl')

    print('Shifting CH areas by 27 days')
    from scipy.interpolate import interp1d
    seconds_since_start = np.array([(date - bayes_dates[0]).total_seconds() for date in bayes_dates])
    ch_areas_interp = interp1d(seconds_since_start, ch_areas, bounds_error=False)

    from scipy.stats import pearsonr
    def mse(x, y):
        return np.nanmean((x-y)**2)
    offset_results = np.zeros((100, 2))
    for i, offset in enumerate(np.linspace(25, 36, offset_results.shape[0])):
        offset_results[i,0] = offset
        x_normal = np.linspace(0, (bayes_dates[-1] - bayes_dates[0]).total_seconds(), len(bayes_dates))
        x_offset = x_normal+offset*24*3600
        ch_areas_normal = ch_areas_interp(x_normal)
        ch_areas_offset = ch_areas_interp(x_offset)
        mask = np.isfinite(ch_areas_normal) & np.isfinite(ch_areas_offset)
        offset_results[i,1] = mse(ch_areas_normal[mask], ch_areas_offset[mask])

    best_i = np.nanargmin(np.abs(offset_results[:,1]))
    print('Best offset at {} with MSE {}'.format(offset_results[best_i,0], offset_results[best_i,1]))

    from datetime import timedelta
    plt.figure()
    x = np.linspace(0, (bayes_dates[-1] - bayes_dates[0]).total_seconds(), len(bayes_dates))
    plt.plot([bayes_dates[0] + timedelta(seconds=seconds) for seconds in x], ch_areas_interp(x), label='Original time')
    plt.plot([bayes_dates[0] + timedelta(seconds=seconds+offset_results[best_i,0]*24*3600) for seconds in x], ch_areas_interp(x), label='+{:.2f} days'.format(offset_results[best_i,0]))
    plt.xlabel('Time')
    plt.ylabel('Area coverage')
    plt.gcf().autofmt_xdate()
    plt.legend(loc='best')
    plt.savefig('/output/plots/ch_areas_shifted.pdf', dpi=600)

    print("Gathering intensity and latitude results")

    original_paths = sorted(glob('/input/originals_4y_1d/*.fits'))
    preprocessed_paths = sorted(glob('/output/preprocessed/4y_1d/*.jl'))
    result_paths = sorted(glob('/output/map/4y_1d/*'))

    if np.any([not os.path.exists('/output/histogram2d_{}.jl'.format(n)) for n in ['AR', 'QS', 'CH']]):
        if np.any([not os.path.exists('/output/{}_4y_1d.dat'.format(n)) for n in ['intensities', 'latitudes', 'segmentations']]):
            intensities = np.memmap('/output/intensities_4y_1d.dat', dtype=np.float64, shape=(len(result_paths), 1024, 1024), mode='w+')
            latitudes = np.memmap('/output/latitudes_4y_1d.dat', dtype=np.float64, shape=(len(result_paths), 1024, 1024), mode='w+')
            segmentations = np.memmap('/output/segmentations_4y_1d.dat', dtype=np.int8, shape=(len(result_paths), 1024, 1024), mode='w+')
            for i, path in enumerate(result_paths):
                intensities[i] = np.log(load(preprocessed_paths[i]))
                latitudes[i] = load(os.path.join(path, 'latitudes.jl'))
                segmentations[i] = load(os.path.join(path, 'segmentation.jl'))

            nan_mask = np.isnan(intensities)
            intensities[nan_mask] = -100
            latitudes[nan_mask] = -100
            del nan_mask

            del intensities
            del latitudes
            del segmentations
        
        intensities = np.memmap('/output/intensities_4y_1d.dat', dtype=np.float64, shape=(len(result_paths), 1024, 1024), mode='r')
        latitudes = np.memmap('/output/latitudes_4y_1d.dat', dtype=np.float64, shape=(len(result_paths), 1024, 1024), mode='r')
        segmentations = np.memmap('/output/segmentations_4y_1d.dat', dtype=np.int8, shape=(len(result_paths), 1024, 1024), mode='r')

        for name, index in ('AR', 0), ('QS', 1), ('CH', 2):
            print('Calculating 2D histogram of intensity and latitude for {}'.format(name))
            mask = segmentations == index
            hist, xedges, yedges = np.histogram2d(intensities[mask], latitudes[mask], bins=1024, range=[[-5, 0], [-90, 90]], normed=True)
            del mask
            dump((hist, xedges, yedges), '/output/histogram2d_{}.jl'.format(name), compress=3)
    hists = np.zeros((3, 1024, 1024))
    for i, name in enumerate(['AR', 'QS', 'CH']):
        hists[i], xedges, yedges = load('/output/histogram2d_{}.jl'.format(name))
    extent =  list(yedges[1:][[0,-1]]) + list(np.exp(xedges[1:])[[0,-1]])

    fig, axes = plt.subplots(ncols=3, sharex=True, sharey=True, figsize=(8,4))
    for i in range(hists.shape[0]):
        axes[i].set_title(['AR','QS','CH'][i])
        im = axes[i].imshow(hists[i], extent=extent, origin='lower')
        axes[i].set_yscale('log')
        axes[i].set_ylim([np.exp(xedges[1]), 1])
        axes[i].set_xlim([-90, 90])
        axes[i].set_xlabel('Latitude (degrees)')
        fig.colorbar(im, ax=axes[i])
    axes[0].set_ylabel('Normalized intensity')
    plt.tight_layout()
    plt.savefig('/output/plots/intensity_latitude_given_segmentation.pdf')

    fig, axes = plt.subplots(ncols=3, sharex=True, sharey=True, figsize=(8,4))
    extent = [-90, 90] + list(np.exp(xedges[1:])[[0,-1]])
    distributions = []
    for i in range(3):
        x = np.exp(np.linspace(np.log(extent[2]), np.log(extent[3]), 1024))
        interp = intensity_kernels[i]
        intensity_likelihood = intensity_kernels[i](x)
        a, b = np.meshgrid(latitude_likelihoods[i], intensity_likelihood)
        distributions.append(a*b)
    for i in range(3):
        axes[i].set_title(['AR','QS','CH'][i])
        im = axes[i].imshow(distributions[i], vmin=0, vmax=np.max(distributions[i]), extent=extent, origin='lower')
        axes[i].set_yscale('log')
        axes[i].set_xlabel('Latitude (degrees)')
        axes[i].set_ylim(extent[2:])
        fig.colorbar(im, ax=axes[i])
    axes[0].set_ylabel('Normalized intensity')
    plt.tight_layout()
    plt.savefig('/output/plots/intensity_latitude_given_class.pdf')


    print('Making segmentations for performance evaluation')
    prefixes = [os.path.basename(path)[:-4] for path in sorted(glob('test_masks/'+'[0-9]'*8+'_'+'[0-9]'*6+'.png'))]
    
    class_index = {"ar": 0, "qs": 1, "ch": 2}
    true_segmentation = np.zeros((len(prefixes), 1024, 1024), dtype=np.int8)
    true_segmentation[:,:,:] = -1
    predicted_segmentation = np.zeros((len(prefixes), 1024, 1024), dtype=np.int8)
    predicted_segmentation[:,:,:] = -1
    memberships = np.zeros((len(prefixes), 1024, 1024, 3), dtype=np.float64)
    
    import skimage.io

    for i, prefix in enumerate(prefixes):
        preprocessed = load("/output/preprocessed/4y_1d/{}.jl".format(prefix))
        for j, class_name in enumerate(['ar', 'qs', 'ch']):
            mask = skimage.io.imread('test_masks/{}.xcf.{}.png'.format(prefix, class_name))[:,:,3] > 0
            true_segmentation[i,mask] = j
        segmentation(memory, '/input/originals_4y_1d/AIA{}_0193.fits'.format(prefix[:-2]), '/output/preprocessed/4y_1d/{}.jl'.format(prefix), '/output/evaluation_segmentations/{}'.format(prefix), my_intensity_kernels_path, class_likelihoods, latitude_likelihoods, 0, 1, ['contours', 'membership', 'segmentation'])
        predicted_segmentation[i] = load('/output/evaluation_segmentations/{}/segmentation.jl'.format(prefix))
        memberships[i] = load('/output/evaluation_segmentations/{}/membership.jl'.format(prefix))
        plt.figure(figsize=(4,4))
        plt.imshow(preprocessed**plot_gamma, cmap='gray')
        plt.contour((predicted_segmentation[i] != true_segmentation[i]) & (true_segmentation[i] >= 0) & (predicted_segmentation[i] >= 0), [0.5], colors='y')
        plt.contour(true_segmentation[i] == 0, [0.5], colors='r')
        plt.contour(true_segmentation[i] == 1, [0.5], colors='g')
        plt.contour(true_segmentation[i] == 2, [0.5], colors='c')
        plt.gca().set_axis_off()
        plt.tight_layout(pad=0, h_pad=0, w_pad=0)
        plt.subplots_adjust(wspace=-0.2, hspace=-0.2, bottom=-0.05, left=-0.05, top=1.05, right=1.05)
        plt.savefig("/output/evaluation_segmentations/{}/plot.pdf".format(prefix), dpi=600)

    from sklearn.metrics import classification_report, confusion_matrix
    valid_mask = (true_segmentation >= 0) & (predicted_segmentation >= 0)
    print(classification_report(true_segmentation[valid_mask], predicted_segmentation[valid_mask]))
    print(confusion_matrix(true_segmentation[valid_mask], predicted_segmentation[valid_mask]))

    print('Analyzing wrongly classified pixels')
    from scipy.stats import gaussian_kde
    kde = gaussian_kde(np.max(memberships, axis=3)[(true_segmentation != predicted_segmentation) & valid_mask])
    x = np.linspace(0, 1, 1024)
    plt.figure()
    plt.plot(x, kde(x))
    plt.xlabel('Normalized intensity')
    plt.ylabel('Probability density function')
    plt.savefig('/output/plots/wrong_pixels_memberships.pdf', dpi=600)
