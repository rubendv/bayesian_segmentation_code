import glob
import random
import shutil
import os

if __name__ == "__main__":
    fits_paths = sorted(glob.glob("/data/AIA/synoptic/2010-10_2014-10_1d/*.fits"))
    preprocessed_paths = sorted(glob.glob("/data/preprocessed_4y_1d/*.png"))
    assert(len(fits_paths) == len(preprocessed_paths))
    assert(not os.path.exists("test_masks"))
    os.mkdir("test_masks")
    for i in sorted(random.sample(filter(lambda x: x != 928, range(len(fits_paths))), 10)):
        shutil.copy(fits_paths[i], "test_masks/")
        shutil.copy(preprocessed_paths[i], "test_masks/")

