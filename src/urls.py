from datetime import timedelta, datetime
import os

if __name__ == "__main__":
    urls = []

    start = datetime(2011, 3, 1, 0, 0, 0)
    current = start
    offset = timedelta(hours=1)
    end = datetime(2011, 4, 1, 0, 0, 0)

    dates = []
    while current < end:
        dates.append(current)
        current = current + offset

    urls = [d.strftime("http://jsoc.stanford.edu/data/aia/synoptic/%Y/%m/%d/H%H00/AIA%Y%m%d_%H%M_0193.fits") for d in dates]
    paths = [d.strftime("/data/AIA/synoptic/2013_daily/AIA%Y%m%d_%H00_0193.fits") for d in dates]

    for url, path in zip(urls, paths):
        print url
