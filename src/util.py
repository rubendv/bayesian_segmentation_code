import os, errno
import sunpy.map
from datetime import datetime
import sunpy.wcs
import numpy as np
from dfresampling.projections import convert_pixel_to_hpc, compose, partial, convert_cartesian_to_polar, convert_hpc_to_hg, convert_sin_hg_pixel_to_hpc_pixel
from dfresampling import map_coordinates, map_coordinates_direct

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise

# Wrapper necessary to use memoized functions in parallel
def memory_wrapper(memory, function, *args, **kwargs):
    return memory.cache(function)(*args, **kwargs)

def get_date(path):
    date_str = sunpy.map.Map(path).date
    date_str = date_str[:date_str.rfind('.')]
    return datetime.strptime(date_str, '%Y-%m-%dT%H:%M:%S')    

def spoca_filling_factor(path):
    m = sunpy.map.Map(path)
    scale = (m.scale['x']*4.0, m.scale['y']*4.0)
    reference_pixel = (m.reference_pixel['x']/4.0, m.reference_pixel['y']/4.0)
    reference_coordinate = (m.reference_coordinate['x'], m.reference_coordinate['y'])
    segmentation = (m.data > 0)[::4,::4].astype(np.float64)
    sin_segmentation = np.zeros_like(segmentation)
    Ci = partial(convert_sin_hg_pixel_to_hpc_pixel, sin_segmentation.shape, (-90, 90), (-90, 90), scale, reference_pixel, reference_coordinate, 0.0, 0.0, float(m.dsun), "arcsec", True, 0.0)
    map_coordinates_direct(segmentation, sin_segmentation, Ci)
    return np.nansum(sin_segmentation > 0.5).astype(np.float64)/np.nansum(np.isfinite(sin_segmentation)).astype(np.float64)

def get_exposure_time(path):
    return sunpy.map.Map(path).exposure_time

def is_anomaly(path):
    m = sunpy.map.Map(path)
    return sunpy.map.Map(path).exposure_time < 1.5 or m.meta['quality'] != 0
