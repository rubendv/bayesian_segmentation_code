import matplotlib
matplotlib.use("Agg")
import sunpy.map
from glob import glob
import sys
import matplotlib.pyplot as plt
import numpy as np
import os
import sunpy.cm
from dfresampling.projections import convert_polar_pixel_to_hpc_pixel, convert_hpc_pixel_to_polar_pixel, convert_range, partial, compose, convert_hg_pixel_to_hpc_pixel, convert_hpc_pixel_to_hg_pixel
from dfresampling import map_coordinates, map_coordinates_direct
from matplotlib import colors
from scipy.ndimage import uniform_filter
import skimage.io
from matplotlib.cm import ScalarMappable
import scipy.stats
from joblib import Memory, Parallel, delayed, dump, load
from util import mkdir_p, memory_wrapper
from datetime import datetime
from util import get_date

def preprocess(original_path, destination_prefix, profile_shelved, vmin=None, vmax=None, upper_percentile=99.9):
    profile = profile_shelved.get()
    polar_profile = np.zeros((len(profile), len(profile)))
    polar_profile[:,:] = profile[None,:]

    m = sunpy.map.Map(original_path)
    image = m.data.astype(np.float64)
    hpc_profile = np.zeros(image.shape)

    scale = (m.scale["x"], m.scale["y"])
    reference_pixel = (m.reference_pixel["x"], m.reference_pixel["y"])
    reference_coordinate = (m.reference_coordinate["x"], m.reference_coordinate["y"])

    Ci = partial(convert_hpc_pixel_to_polar_pixel, polar_profile.shape, m.rsun_arcseconds*max_radius, scale, reference_pixel, reference_coordinate)
    map_coordinates_direct(polar_profile, hpc_profile, Ci, y_cyclic=True, out_of_range_nan=True)#, max_samples_width=32, singularities_nan=False)

    image /= hpc_profile
    if vmin is None:
        vmin = 0.0
    if vmax is None:
        vmax = np.percentile(np.ma.masked_invalid(image).compressed(), upper_percentile)
    image = np.clip((image - vmin) / (vmax - vmin), 0, 1)
    if destination_prefix is not None:
        dump(image, destination_prefix+'.jl', compress=3)
        preview(image, destination_prefix+'.png')
    return destination_prefix, (vmin, vmax)

def preview(image, destination_path):
    cmap = sunpy.cm.get_cmap("sdoaia193")
    skimage.io.imsave(destination_path, cmap(image))

def image_profile(path, max_radius=1.0):
    m = sunpy.map.Map(path)
    hpc = m.data.astype(np.float64)

    scale = (m.scale["x"], m.scale["y"])
    reference_pixel = (m.reference_pixel["x"], m.reference_pixel["y"])
    reference_coordinate = (m.reference_coordinate["x"], m.reference_coordinate["y"])

    polar = np.zeros((1024, 1024))
    Ci = partial(convert_polar_pixel_to_hpc_pixel, polar.shape, m.rsun_arcseconds*max_radius, scale, reference_pixel, reference_coordinate)
    map_coordinates(hpc, polar, Ci, y_cyclic=True)
    return scipy.stats.nanmedian(polar, axis=0)

def global_profile(memory, paths, restricted=True, max_radius=1.0):
    profiles = np.array(Parallel(n_jobs=-1, backend='threading', verbose=50)([delayed(memory_wrapper)(memory, image_profile, path, max_radius) for path in paths if not restricted or memory.cache(get_date)(path) < datetime(2011, 3, 1, 0, 0, 0)]))
    return scipy.stats.nanmedian(profiles, axis=0)

if __name__ == '__main__':
    cache_path = '/output/cache'
    mkdir_p(cache_path)
    memory = Memory(cachedir=cache_path, verbose=0, compress=False)

    paths = {}
    for name in ['4y_1d', '3m_1h']:
        paths[name] = sorted(glob("/input/originals_{0}/*.fits".format(name)))

    # Calculate limb profiles
    max_radius = 1.0
    print("Calculating radial intensity profile for unrestricted dataset")
    profile_unrestricted = memory.cache(global_profile)(memory, paths['4y_1d'], restricted=False, max_radius=max_radius)
    print("Calculating radial intensity profile for restricted dataset")
    profile_shelved = memory.cache(global_profile).call_and_shelve(memory, paths['4y_1d'], restricted=True, max_radius=max_radius)
    profile = profile_shelved.get()

    # Plot limb profiles
    mkdir_p("/output/plots")
    plt.figure()
    plt.title("Radial intensity profile")
    plt.plot(np.linspace(0, max_radius, len(profile_unrestricted)), profile_unrestricted, label="Full dataset")
    plt.plot(np.linspace(0, max_radius, len(profile)), profile, label="Restricted dataset")
    plt.xlabel("Radius (observed solar radii)")
    plt.ylabel("Median intensity (DN/s)")
    plt.legend(loc="best")
    plt.tight_layout()
    plt.savefig("/output/plots/radial_profile.pdf")

    _, (vmin, vmax) = preprocess(original_path=paths['4y_1d'][0], destination_prefix=None, profile_shelved=profile_shelved, vmin=None, vmax=None)
    
    for dataset_name, dataset_paths in paths.items():
        directory = '/output/preprocessed/{name}'.format(name=dataset_name)
        mkdir_p(directory)
        print("Preprocessing files for {} dataset".format(dataset_name))
        Parallel(n_jobs=-1, verbose=50)([delayed(memory_wrapper)(
            memory, 
            preprocess, 
            original_path=path, 
            destination_prefix=os.path.join(directory, memory.cache(get_date)(path).strftime('%Y%m%d_%H%M%S')),
            profile_shelved=profile_shelved, 
            vmin=vmin, 
            vmax=vmax
        ) for path in dataset_paths])
