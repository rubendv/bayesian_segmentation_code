import numpy as np
from skimage.io import imread
from scipy.stats import gaussian_kde
import sunpy.cm
import scipy.interpolate
from glob import glob
import sunpy.map
from dfresampling.projections import convert_pixel_to_hpc, compose, partial, convert_cartesian_to_polar, convert_hpc_to_hg, convert_sin_hg_pixel_to_hpc_pixel
from dfresampling import map_coordinates, map_coordinates_direct
import sys
import os
import random
import matplotlib.pyplot as plt
from skimage import morphology
from scipy.optimize import fsolve
import os, errno
from joblib import Memory, Parallel, delayed, dump, load
from preprocess import get_date
from util import mkdir_p, memory_wrapper
import dill
from sklearn.neighbors import KernelDensity

def interpolate_latitude_likelihoods(latitude_likelihoods):
    num_classes = len(latitude_likelihoods)
    return [scipy.interpolate.interp1d(
        np.linspace(-90, 90, latitude_likelihoods.shape[1]), 
        latitude_likelihoods[i], 
        bounds_error=True,
        assume_sorted=True
    ) for i in range(num_classes)]

def get_hg_polar_coords_and_properties(original_path, shape):
    m = sunpy.map.Map(original_path)
    scale = (m.scale["x"], m.scale["y"])
    reference_pixel = (m.reference_pixel["x"], m.reference_pixel["y"])
    reference_coordinate = (m.reference_coordinate["x"], m.reference_coordinate["y"])
    pixel_coords = np.zeros(shape + (2, ))
    pixel_coords[...,0], pixel_coords[...,1] = np.meshgrid(np.arange(shape[1]).astype(np.float64), np.arange(shape[0]).astype(np.float64))
    hpc_coords = convert_pixel_to_hpc(scale, reference_pixel, reference_coordinate, pixel_coords)
    polar_coords = convert_cartesian_to_polar(hpc_coords)
    hg_coords = convert_hpc_to_hg(m.heliographic_latitude, 0.0, m.dsun, "arcsec", hpc_coords)
    hg_coords[hg_coords < -90] += 180
    hg_coords[hg_coords > 90] -= 180

    return hg_coords, polar_coords, scale, reference_pixel, reference_coordinate, m.dsun, m.rsun_arcseconds, m.heliographic_latitude

def class_decision(memberships, threshold=0.0):
    memberships = np.ma.masked_invalid(memberships).filled(-1)
    ml = np.argmax(memberships, axis=2)
    segmentation = np.zeros((memberships.shape[0], memberships.shape[1]), dtype=np.int8)
    segmentation[:,:] = 1
    segmentation[(ml == 0) & (memberships[:,:,0] > threshold)] = 0
    segmentation[(ml == 2) & (memberships[:,:,2] > threshold)] = 2
    return segmentation

def segmentation(memory, original_path, preprocessed_path, destination_prefix, intensity_kernels_path, class_likelihoods, latitude_likelihoods, intensity_vmin, intensity_vmax, properties_to_save=None, calculate_likelihoods=False):
    if properties_to_save is None:
        properties_to_save = []
    with open(intensity_kernels_path, 'r') as f:
        intensity_kernels = dill.load(f)
    num_classes = len(class_likelihoods)
    if len(intensity_kernels) != num_classes or len(latitude_likelihoods) != num_classes:
        raise ValueError('number of likelihood functions must be the same for all types of likelihood functions')

    if len(properties_to_save) > 0:
        mkdir_p(destination_prefix)

    latitude_likelihood_pdfs = interpolate_latitude_likelihoods(latitude_likelihoods)
    image = np.clip(load(preprocessed_path), intensity_vmin, intensity_vmax)
    hg_coords, polar_coords, scale, reference_pixel, reference_coordinate, dsun, rsun_arcseconds, b0 = memory.cache(get_hg_polar_coords_and_properties)(original_path, image.shape)

    membership = np.zeros(image.shape + (num_classes,))
    for class_i, kernel in enumerate(intensity_kernels):
        membership[:,:,class_i] = kernel(image.flatten()).reshape(image.shape) * class_likelihoods[class_i] * latitude_likelihood_pdfs[class_i](hg_coords[:,:,1].flatten()).reshape(image.shape)
    membership /= np.sum(membership, axis=2)[:,:,None]
    
    if 'areas' in properties_to_save or calculate_likelihoods:
        Ci = partial(convert_sin_hg_pixel_to_hpc_pixel, image.shape, (-90, 90), (-90, 90), scale, reference_pixel, reference_coordinate, 0.0, 0.0, dsun, "arcsec", True, 0.0)
        sin_membership = np.zeros_like(membership)
        for class_i in range(num_classes):
            map_coordinates_direct(membership[...,class_i], sin_membership[...,class_i], Ci)
        sin_segmentation = class_decision(sin_membership)
        sin_segmentation[np.isnan(sin_membership[...,0])] = -1

        areas = np.zeros(num_classes)
        for i in range(num_classes):
            areas[i] = np.sum(sin_segmentation == i)
        areas /= np.sum(areas)
        class_likelihoods = areas
    if 'segmentation' in properties_to_save or 'contours' in properties_to_save or calculate_likelihoods:
        segmentation = class_decision(membership)
        segmentation[np.isnan(membership[...,0])] = -1
    if 'latitudes' in properties_to_save or calculate_likelihoods:
        latitudes = hg_coords[:,:,1]
        latitude_likelihoods = np.zeros_like(latitude_likelihoods)
        range_mask = (latitudes >= -90) & (latitudes <= 90)
        for i in range(num_classes):
            samples = latitudes[(segmentation == i) & range_mask].flatten()
            if len(samples) > 1000:
                ## Scott's Rule: n**(-1./(d+4)) with n number of data points and d number of dimensions
                #bandwidth = float(len(samples))**(-1./(1+4))
                #kde = KernelDensity(bandwidth=bandwidth, kernel='gaussian').fit(samples)
                #latitude_likelihoods[i] = np.exp(kde.score_samples(np.linspace(-90, 90, latitude_likelihoods.shape[1])[:, np.newaxis]))
                kde = gaussian_kde(np.random.choice(samples, 1000))
                latitude_likelihoods[i] = kde(np.linspace(-90, 90, latitude_likelihoods.shape[1]))
            else:
                latitude_likelihoods[i] = np.nan
    if 'segmentation' in properties_to_save:
        dump(segmentation, os.path.join(destination_prefix, 'segmentation.jl'), compress=True)
    if 'membership' in properties_to_save:
        dump(membership, os.path.join(destination_prefix, 'membership.jl'), compress=True)
    if 'latitudes' in properties_to_save:
        dump(latitudes, os.path.join(destination_prefix, 'latitudes.jl'), compress=True)
    if 'areas' in properties_to_save:
        dump(areas, os.path.join(destination_prefix, 'areas.jl'), compress=True)
    if 'contours' in properties_to_save:
        plt.clf()
        plt.gcf().set_size_inches((8,8))
        plt.imshow(image, cmap=sunpy.cm.get_cmap("sdoaia193"), origin="lower")
        plt.contour(segmentation == 0, [0.5], colors='r', linewidths=1)
        plt.contour(segmentation == 2, [0.5], colors='c', linewidths=1)
        plt.savefig(os.path.join(destination_prefix, "contours.png"), dpi=1024/8)

    if calculate_likelihoods:
        return class_likelihoods, latitude_likelihoods
    else:
        return None

def segment_dataset(memory, original_paths, preprocessed_paths, directory, intensity_kernels_path, class_likelihoods, latitude_likelihoods, intensity_vmin, intensity_vmax, properties_to_save=None, calculate_likelihoods=False):
    results = Parallel(n_jobs=-1)([delayed(memory_wrapper)(
        memory, segmentation, memory, original_path, preprocessed_path, os.path.join(directory, memory_wrapper(memory, get_date, original_path).strftime('%Y%m%d_%H%M%S')), intensity_kernels_path, class_likelihoods, latitude_likelihoods, intensity_vmin, intensity_vmax, properties_to_save, calculate_likelihoods
    ) for (original_path, preprocessed_path) in zip(sorted(original_paths), sorted(preprocessed_paths))])
    
    return results
