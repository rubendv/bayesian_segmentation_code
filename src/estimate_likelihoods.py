import numpy as np
from skimage.io import imread
from scipy.stats import gaussian_kde
import sunpy.cm
import scipy.interpolate
from glob import glob
import sunpy.map
from dfresampling.projections import convert_pixel_to_hpc, compose, partial, convert_cartesian_to_polar, convert_hpc_to_hg, convert_sin_hg_pixel_to_hpc_pixel
from dfresampling import map_coordinates, map_coordinates_direct
import sys
import os
import random
import matplotlib.pyplot as plt
from segmentation import segmentation
from util import mkdir_p, memory_wrapper, get_date
from joblib import Memory, Parallel, delayed, load, dump
import shutil
import dill
from segmentation import segment_dataset

def load_masks(ar_file, qs_file, ch_file):
    masks = np.zeros((3, 1024, 1024), dtype=bool)
    masks[0] = imread(ar_file)[:,:,3] > 0
    masks[1] = imread(qs_file)[:,:,3] > 0
    masks[2] = imread(ch_file)[:,:,3] > 0
    return masks

def calculate_kernels(masks, preprocessed):
    masked = np.ma.masked_invalid(preprocessed)
    compressed = masked.compressed()

    vmin = 0
    vmax = 1
    x = np.linspace(vmin, vmax, 1024)
    kernels = [scipy.interpolate.interp1d(x, gaussian_kde(masked[masks[i]].compressed())(x)) for i in range(len(masks))]
    
    return kernels, vmin, vmax

def em_iteration(memory, original_paths, preprocessed_paths, destination_prefix, intensity_kernels_path, intensity_vmin, intensity_vmax, disable_latitudes=False, max_iterations=100):
    if(len(original_paths) != len(preprocessed_paths)):
        raise ValueError("original_paths and preprocessed_paths must have the same length")

    mkdir_p(destination_prefix)
    
    with open(intensity_kernels_path, 'r') as f:
        intensity_kernels = dill.load(f)
    num_classes = len(intensity_kernels)
    class_likelihoods = np.ones(num_classes)/num_classes
    latitude_likelihoods = np.ones((num_classes, 512))/180

    for i in range(max_iterations):
        directory = os.path.join(destination_prefix, '{:08}'.format(i))
        mkdir_p(directory)
        results = segment_dataset(memory, original_paths, preprocessed_paths, directory, intensity_kernels_path, class_likelihoods, latitude_likelihoods, intensity_vmin, intensity_vmax, calculate_likelihoods=True)
        
        # Convert list of pairs to pair of lists
        results = zip(*results)
        
        # Calculate aggregate likelihood functions
        new_class_likelihoods = np.nanmean(np.array(results[0]), axis=0)
        new_latitude_likelihoods = np.nanmean(np.array(results[1]), axis=0)
        
        delta = (new_class_likelihoods - class_likelihoods)/class_likelihoods
        
        class_likelihoods = new_class_likelihoods
        if not disable_latitudes:
            latitude_likelihoods = new_latitude_likelihoods
        dump(class_likelihoods, os.path.join(directory, 'class_likelihoods.jl'), compress=True)
        dump(latitude_likelihoods, os.path.join(directory, 'latitude_likelihoods.jl'), compress=True)
        plt.clf()
        for j in range(num_classes):
            plt.plot(np.linspace(-90, 90, latitude_likelihoods.shape[1]), latitude_likelihoods[j], ['r-', 'g-', 'c-'][j])
        plt.xlim([-90, 90])
        plt.savefig(os.path.join(directory, 'latitude_likelihoods.pdf'))
        print("Iteration {} class likelihoods: AR: {cl[0]:.2%}, QS: {cl[1]:.2%}, CH: {cl[2]:.2%}, delta: AR: {delta[0]:.2%}, QS: {delta[1]:.2%}, CH: {delta[2]:.2%}".format(i, cl=class_likelihoods, delta=delta))
        if np.max(np.abs(delta)) < 1e-2:
            print("Convergence reached")
            break
    return class_likelihoods, latitude_likelihoods

