#!/usr/bin/env bash

for xcf_file in test_masks/*.xcf
do
    xcf2png $xcf_file "AR" > "${xcf_file}.ar.png"
    xcf2png $xcf_file "QS" > "${xcf_file}.qs.png"
    xcf2png $xcf_file "CH" > "${xcf_file}.ch.png"
    echo $xcf_file
done
