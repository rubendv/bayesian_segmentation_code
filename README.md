# README #

This repository contains the source code for the paper "Supervised Classification of Solar Features using Prior Information" by De Visscher et al, submitted to the Journal of Space Weather and Space Climate in 2015.

The file `src/reproduce_results.py` reproduces the results of this paper if the necessary datasets are present.
These can be obtained by contacting the author at `ruben.devisscher@observatory.be`.

Use git to clone this repository on your computer, or see [Downloads](https://bitbucket.org/rubendv/bayesian_segmentation_code/downloads) to download a zip of the repository.