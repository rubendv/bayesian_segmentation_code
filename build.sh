#!/usr/bin/env bash

docker build -t bayesian_segmentation . && docker tag -f bayesian_segmentation rubendv-pc:5000/bayesian_segmentation && docker push rubendv-pc:5000/bayesian_segmentation
